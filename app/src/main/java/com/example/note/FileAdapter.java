package com.example.note;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.BookVh> {

    Context context ;
    List<File> files;

    public FileAdapter(Context context , List<File> files) {
        this.context = context;
        this.files = files;
    }

    @NonNull
    @Override
    public BookVh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(context).inflate(R.layout.activity_row_note , parent , false);
        return new BookVh(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookVh holder, int position) {
        holder.setData(files.get(position));
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    class BookVh extends RecyclerView.ViewHolder{
        TextView  file_name, file_last_update;
        RelativeLayout row;
        public BookVh(@NonNull View itemView) {
            super(itemView);
            file_name = itemView.findViewById(R.id.name);
            file_last_update = itemView.findViewById(R.id.last_update);
            row = itemView.findViewById(R.id.note_row);
        }

        public void setData(File file) {

            file_name.setText(file.getName());
            String s = new SimpleDateFormat("yyyy/mm/dd").format(new Date(file.lastUpdate) );
            file_last_update.setText(s.equals("") || s == null ? "not found" : s);
            row.setBackgroundColor(file.color);

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ViewNote.class);
                    Bundle b = new Bundle();
                    b.putBoolean("edit", true);
                    b.putString("name", file.name);
                    b.putInt("color", file.color);
                    b.putLong("lastUpdate", file.lastUpdate);
                    b.putString("id", file.id);
                    i.putExtras(b);
                    context.startActivity(i);
                }
            });
        }
    }
}
