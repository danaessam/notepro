package com.example.note;

public class Notes {


        String id;
        String title ;
        String desc;
        String authorName;
        long createdAt;
        long lastUpdate;
        int color;


        public Notes(){}

        public Notes(String id, String title, String desc, String authorName, long createdAt, long lastUpdate, int color) {
            this.id = id;
            this.title = title;
            this.desc = desc;
            this.createdAt = createdAt;
            this.lastUpdate = lastUpdate;
            this.authorName = authorName;
            this.color = color;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public long getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(long createdAt) {
            this.createdAt = createdAt;
        }

        public long getLastUpdate() {
            return lastUpdate;
        }

        public void setLastUpdate(long lastUpdate) {
            this.lastUpdate = lastUpdate;
        }
    }


