package com.example.note;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class signup extends AppCompatActivity {

    EditText email , password;
    Button signbtn;

    FirebaseAuth Auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Auth = FirebaseAuth.getInstance();

        FirebaseUser user = Auth.getCurrentUser();
        if (user != null) {
            Intent intent = new Intent(signup.this, browse.class);
            startActivity(intent);

        }
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        findViewById(R.id.move_to_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(signup.this, login.class));
            }
        });
        signbtn = findViewById(R.id.signbtn);
        signbtn.setOnClickListener(v->{
            doSignUp(email.getText().toString() , password.getText().toString());
        });

    }

    private void doSignUp(String email, String password) {
        Auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseUser user = Auth.getCurrentUser();
                        String uemail = user.getEmail();
                        String uid = user.getUid();
                        Map<String,Object> data = new HashMap<>();
                        data.put("uid",uid);
                        data.put("email",uemail);
                        data.put("createdAt",new Date().getTime());


                        FirebaseDatabase.getInstance().getReference().child("User").child(uid).setValue(data)
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(signup.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                        Log.d("error",e.getLocalizedMessage());
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Intent intent = new Intent(signup.this , browse.class);
                                        startActivity(intent);
                                    }
                                });








                    }
                    else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(signup.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }

                });
    }
}
