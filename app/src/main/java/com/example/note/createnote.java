package com.example.note;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class createnote extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createnote);
    }

    public void create_note(View view) {
        Intent createnote=new Intent(createnote.this,addnoteinfo.class);
        startActivity(createnote);
    }

    public void done2note(View view) {
        Intent done=new Intent(createnote.this,note.class);
        startActivity(done);
    }
}
