package com.example.note;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class newfile extends AppCompatActivity {
    File file;
    EditText name;
    LinearLayout activity;
    int color;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newfile);

        file = new File();
        name = findViewById(R.id.name);
        Bundle b = getIntent().getExtras();
        activity = findViewById(R.id.newfile);
        color = 0;

        findViewById(R.id.pur_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(NoteAddActivity.this, "scbjaj", Toast.LENGTH_LONG).show();
                activity.setBackgroundColor(Color.MAGENTA);
                color = Color.MAGENTA;
            }
        });

        (findViewById(R.id.red_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.newfile).setBackgroundColor(Color.RED);
                color = Color.RED;
            }
        });

        (findViewById(R.id.blue_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.newfile).setBackgroundColor(Color.BLUE);
                color = Color.BLUE;
            }
        });

        (findViewById(R.id.green_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.newfile).setBackgroundColor(Color.GREEN);
                color = Color.GREEN;
            }
        });

        findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean save = b.getBoolean("save");
                if(save){
                    updateFile(b);
                }else {
                    AddFile();
                }
            }
        });
    }

    private void updateFile(Bundle b) {

        file.id = b.getString("id");
        file.color = b.getInt("color") == 0 ? R.color.colorAccent: b.getInt("color");
        file.name = b.getString("name");

        ((Button)findViewById(R.id.save_btn)).setText("update");
        FirebaseDatabase.getInstance().getReference().child("File").child(file.id).setValue(file);
    }

    private void AddFile() {
        file.name = name.getText().toString();

        ((Button)findViewById(R.id.save_btn)).setText("save");
//        String id = FirebaseDatabase.getInstance().getReference().child("File")
//                .push().getKey();
//        file.id = id;

        Toast.makeText(newfile.this, "" + color, Toast.LENGTH_LONG).show();

//        FirebaseDatabase.getInstance().getReference().child("File").child(id).setValue(file);
//        startActivity(new Intent(newfile.this, MainActivity.class));
    }
}
