package com.example.note;

public class File {

    String id;
    String name ;
    Long lastUpdate;
    int color;


    public File(){}

    public File(String id, String name,    long lastUpdate, int color) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.lastUpdate = lastUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }


}


