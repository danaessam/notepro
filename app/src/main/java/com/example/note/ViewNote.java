package com.example.note;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class ViewNote extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_note);
        final Bundle b = getIntent().getExtras();

        ((TextView)findViewById(R.id.tv_title)).setText("title : " + b.getString("title"));
        ((TextView)findViewById(R.id.tv_author)).setText("author : " + b.getString("author"));
        ((TextView)findViewById(R.id.tv_desc)).setText("description : " + b.getString("desc"));
        ((TextView)findViewById(R.id.tv_created_at)).setText("create at : " + new SimpleDateFormat("yyyy/mm/dd").format(b.getLong("createAt")));
        ((TextView)findViewById(R.id.tv_last_update)).setText("last update : " + new SimpleDateFormat("yyyy/mm/dd").format(b.getLong("lastUpdate")));
//        Toast.makeText(ViewNoteActivity.this,"" + getIntent().getExtras().getInt("color"), Toast.LENGTH_LONG).show();
        findViewById(R.id.update_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ViewNote.this, addfile.class);
                i.putExtras(b);
                startActivity(i);
            }
        });
    }
}
