package com.example.note;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class forgotpass extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpass);
    }

    public void recover(View view) {
        Intent recover=new Intent(forgotpass.this,checkemail.class);
        startActivity(recover);
    }

    public void retern(View view) {
        Intent retern =new Intent(forgotpass.this,login.class);
        startActivity(retern);
    }
}
