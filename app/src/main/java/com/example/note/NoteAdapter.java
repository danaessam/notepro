package com.example.note;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.BookVh> {

    Context context ;
    List<Notes> notes;

    public NoteAdapter(Context context , List<Notes> notes) {
        this.context = context;
        this.notes = notes;
    }

    @NonNull
    @Override
    public BookVh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(context).inflate(R.layout.activity_row_note , parent , false);
        return new BookVh(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookVh holder, int position) {
        holder.setData(notes.get(position));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    class BookVh extends RecyclerView.ViewHolder{
        TextView  note_name, note_last_update;
        RelativeLayout row;
        public BookVh(@NonNull View itemView) {
            super(itemView);
            note_name = itemView.findViewById(R.id.name);
            note_last_update = itemView.findViewById(R.id.last_update);
            row = itemView.findViewById(R.id.note_row);
        }

        public void setData(Notes note) {

            note_name.setText(note.getTitle());
            String s = new SimpleDateFormat("yyyy/mm/dd").format(new Date(note.lastUpdate) );
            note_last_update.setText(s.equals("") || s == null ? "not found" : s);
            row.setBackgroundColor(note.color);

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ViewNote.class);
                    Bundle b = new Bundle();
                    b.putBoolean("edit", true);
                    b.putString("title", note.title);
                    b.putString("author", note.authorName);
                    b.putString("desc", note.desc);
                    b.putInt("color", note.color);
                    b.putLong("createAt", note.createdAt);
                    b.putLong("lastUpdate", note.lastUpdate);
                    b.putString("id", note.id);
                    i.putExtras(b);
                    context.startActivity(i);
                }
            });
        }
    }
}
