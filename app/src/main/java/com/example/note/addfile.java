package com.example.note;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class addfile extends AppCompatActivity {
    RecyclerView fileList_rv;
    FileAdapter fileAdapter;

    List<File> fileList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addfile);

        initData();


        fileList_rv = findViewById(R.id.file_list);
        fileList_rv.setLayoutManager(new LinearLayoutManager(this));
        fileAdapter = new FileAdapter(this, fileList);
        fileList_rv.setAdapter(fileAdapter);

        findViewById(R.id.add_cate).setOnClickListener(v -> {
            Intent i = new Intent(addfile.this, newfile.class);
            Bundle b = new Bundle();
            b.putBoolean("edit", false);
            i.putExtras(b);
            startActivity(i);
        });

        FirebaseDatabase.getInstance().getReference().child("file").child("-LvZyjvGPh8rLz2SWo3z")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String id = dataSnapshot.child("id").getValue(String.class);
                        String name = dataSnapshot.child("name").getValue(String.class);

                        Toast.makeText(addfile.this, name + " ", Toast.LENGTH_SHORT).show();

                        Log.d("onDataChange", name + " ");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        logout();
        return true;
    }

    private void logout() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(addfile.this, login.class));
    }


    private void initData() {
        FirebaseDatabase.getInstance().getReference().child("file")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        fileList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            File file = snapshot.getValue(File.class);
                            fileList.add(file);
                        }
                        fileAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(addfile.this, "network error", Toast.LENGTH_SHORT).show();
                    }
                });

    }




    public void move(View view) {
        Intent move=new Intent(addfile.this,row_note.class);
        startActivity(move);
    }

    public void addcate(View view) {
        Intent cati=new Intent(addfile.this,newfile.class);
        startActivity(cati);
    }
}
