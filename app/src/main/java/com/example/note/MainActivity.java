package com.example.note;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView notesList_rv;
    NoteAdapter noteAdapter;

    List<Notes> noteList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();


        notesList_rv = findViewById(R.id.note_list);
        notesList_rv.setLayoutManager(new LinearLayoutManager(this));
        noteAdapter = new NoteAdapter(this ,noteList);
        notesList_rv.setAdapter(noteAdapter);

        findViewById(R.id.add_note).setOnClickListener(v->{
            Intent i = new Intent(MainActivity.this, addfile.class);
            Bundle b = new Bundle();
            b.putBoolean("edit", false);
            i.putExtras(b);
            startActivity(i);
        });

        FirebaseDatabase.getInstance().getReference().child("Note").child("-LvZyjvGPh8rLz2SWo3z")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Long createdAt = dataSnapshot.child("createdAt").getValue(Long.class);
                        String desc = dataSnapshot.child("desc").getValue().toString();
                        String id = dataSnapshot.child("id").getValue(String.class);
                        String title = dataSnapshot.child("title").getValue(String.class);

                        Toast.makeText(MainActivity.this, title + " " +desc, Toast.LENGTH_SHORT).show();

                        Log.d("onDataChange",title + " " +desc);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        logout();
        return true;
    }

    private void logout() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(MainActivity.this, login.class));
    }


    private void initData() {
        FirebaseDatabase.getInstance().getReference().child("Note")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        noteList.clear();
                        for(DataSnapshot snapshot: dataSnapshot.getChildren() ){
                            Notes note = snapshot.getValue(Notes.class);
                            noteList.add(note);
                        }
                        noteAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(MainActivity.this, "network error", Toast.LENGTH_SHORT).show();
                    }
                });

    }
    public void done(View view) {
        Intent done=new Intent(MainActivity.this,browse.class);
        startActivity(done);
    }

    public void add2note(View view) {
        Intent add=new Intent(MainActivity.this,addnoteinfo.class);
        startActivity(add);
    }
}
