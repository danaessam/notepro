package com.example.note;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class browse extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse);
    }

    public void edit(View view) {

        Intent edit = new Intent(browse.this,addnoteinfo.class);
        startActivity(edit);
    }

    public void show(View view) {
        Intent show = new Intent(browse.this, addfile.class);
        startActivity(show);
    }

    public void creat(View view) {
        Intent creat=new Intent(browse.this,newfile.class);
        startActivity(creat);
    }



    public void shownote(View view) {
        Intent shownote=new Intent(browse.this,note.class);
        startActivity(shownote);
    }

    public void addn(View view) {
        Intent addn =new Intent(browse.this,addnoteinfo.class);
        startActivity(addn);
    }
}
