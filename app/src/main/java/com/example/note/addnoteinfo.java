package com.example.note;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class addnoteinfo extends AppCompatActivity {


        Notes note;
        EditText title, author, desc;
        LinearLayout activity;
        int color;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_addnoteinfo);

            note = new Notes();
            title = findViewById(R.id.title);
            author = findViewById(R.id.Author);
            desc = findViewById(R.id.desc);
            Bundle b = getIntent().getExtras();
            activity = findViewById(R.id.add_note);
            color = 0;

            findViewById(R.id.pur_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                Toast.makeText(NoteAddActivity.this, "scbjaj", Toast.LENGTH_LONG).show();
                    activity.setBackgroundColor(Color.MAGENTA);
                    color = Color.MAGENTA;
                }
            });

            (findViewById(R.id.red_btn)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.add_note).setBackgroundColor(Color.RED);
                    color = Color.RED;
                }
            });

            (findViewById(R.id.blue_btn)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.add_note).setBackgroundColor(Color.BLUE);
                    color = Color.BLUE;
                }
            });

            (findViewById(R.id.green_btn)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.add_note).setBackgroundColor(Color.GREEN);
                    color = Color.GREEN;
                }
            });

            findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean edit = b.getBoolean("edit");
                    if(edit){
                        updateNote(b);
                    }else {
                        AddNote();
                    }
                }
            });
        }

        private void updateNote(Bundle b) {

            note.id = b.getString("id");
            note.color = b.getInt("color") == 0 ? R.color.colorAccent: b.getInt("color");
            note.title = b.getString("title");
            note.authorName = b.getString("author") == null ? "not found" : b.getString("author");
            note.desc = b.getString("desc");
            note.createdAt = b.getLong("createAt");
            note.lastUpdate = new Date().getTime();

            ((Button)findViewById(R.id.btn_add)).setText("update");
            FirebaseDatabase.getInstance().getReference().child("Note").child(note.id).setValue(note);
        }

        private void AddNote() {
            note.createdAt = new Date().getTime();
            note.lastUpdate = new Date().getTime();
            note.title = title.getText().toString();
            note.desc = desc.getText().toString();
            note.authorName = author.getText().toString();

            ((Button)findViewById(R.id.btn_add)).setText("Add");
//        String id = FirebaseDatabase.getInstance().getReference().child("Note")
//                .push().getKey();
//        note.id = id;

            Toast.makeText(addnoteinfo.this, "" + color, Toast.LENGTH_LONG).show();

//        FirebaseDatabase.getInstance().getReference().child("Note").child(id).setValue(note);
//        startActivity(new Intent(NoteAddActivity.this, MainActivity.class));
        }
    }
