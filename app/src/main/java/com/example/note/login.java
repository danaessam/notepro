package com.example.note;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {

    EditText in_email , in_password;
    Button login_btn;

    FirebaseAuth Auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Auth = FirebaseAuth.getInstance();

        FirebaseUser user = Auth.getCurrentUser();
        if (user!=null)
        {
            Intent intent = new Intent(login.this , browse.class);
            startActivity(intent);
        }

        in_email = findViewById(R.id.in_email);
        in_password = findViewById(R.id.in_password);
        login_btn = findViewById(R.id.login_btn);
        login_btn.setOnClickListener(v->{
            dologIn(in_email.getText().toString() , in_password.getText().toString());
        });



    }
    private void dologIn(String email, String password) {
        Auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseUser user = Auth.getCurrentUser();


                        Map<String,Object> data = new HashMap<>();
                        data.put("lastLogIn",new Date().getTime());


                        FirebaseDatabase.getInstance().getReference().child("User").child(user.getUid()).updateChildren(data)
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(login.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                        Log.d("error",e.getLocalizedMessage());
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Intent intent = new Intent(login.this , browse.class);
                                        startActivity(intent);
                                    }
                                });






                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(login.this, "Login failed.",
                                Toast.LENGTH_SHORT).show();
                    }

                });
    }

    public void forgotpass(View view) {
        Intent forgot=new Intent(login.this,forgotpass.class);
        startActivity(forgot);
    }


}



